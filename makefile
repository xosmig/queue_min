
STD = c++1z
DEBUG_FLAGS = -O0 -Wall -std=$(STD) -D _GLIBCXX_DEBUG -g -Wextra -Wshadow -Wpedantic -Wfatal-errors

BUILD_OUT = g++ $^ -o $@
BUILD_O = g++ -O0 -D NDEBUG -c -Wfatal-errors -std=$(STD) $< -o $@
BUILD_H = touch $@

TEST_OUT = qtestmain.out
TEST_CPP_FILES = test/main.cpp
#TEST_O_FILES = bin/main.o

.PHONY:    test test_release test_exec test_ru    clean edit    default

default: test_debug
#default: test_release

#GENERAL:

clean:
	find \( -name "*.out" -or -name "*.o" -or -name "*.tmp" -or -name "*.log" \) -and -delete

edit:
	subl --project queue.sublime-project

#TEST:

test_debug:
	g++ $(DEBUG_FLAGS) $(TEST_CPP_FILES) -o $(TEST_OUT)

test_release: $(TEST_OUT)

test_exec:
	./$(TEST_OUT)

test_run: clean test_debug test_exec

