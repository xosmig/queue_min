
#include "../source/queue_min_ua.hpp"
#include "../source/queue_min_am.hpp"

#include <iostream>

int main() {
	my::queue_min_ua<int> q;
	q.push(20);
	q.push(15);
	q.push(2);
	q.push(42);
	q.push(17);
	for (int i = 0; i < 5; ++i) {
		std::cout << "front: " << q.front() << std::endl;
		std::cout << "min: " << q.min() << std::endl;
		std::cout << "pop: ";
		q.pop();
		std::cout << "ok\n" << std::endl;
	}
}