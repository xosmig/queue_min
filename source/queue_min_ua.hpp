
#ifndef _QUEUE_MIN_UA_HPP_
#define _QUEUE_MIN_UA_HPP_

#include <deque>
#include <stack>
#include <utility>
#include <memory>

#include <iostream>

namespace my {

	template<
		class T,
		class Compare = std::less<T>,
		class Stack = std::deque<std::pair<const T, const T>>, // must have: emplace_back, back, pop_back, clear, empty
		class Deque = std::deque<std::pair<const T, const T>> // must have: emplace_back, back, pop_back, front, pop_front, clear, empty
	>
	class queue_min_ua {
		Stack inp_obj_1, inp_obj_2, *inp, *inp_tmp;
		Deque out_obj_1, out_obj_2, *out, *out_tmp;

		Compare comp;

		enum mode {
			common,
			itmp_otmp,
			out_otmp
		} cur_mode;

		const T &min(const T &x, const T &y) const { // min of 2
			if (comp(x, y)) {
				return x;
			} else {
				return y;
			}
		}

		void push_out(Deque *d, const T &obj) { // inline by default
			d->emplace_back(obj, (d->empty() ? obj :min(obj, (d->back()).second)));
		}

		void push_inp(Stack *s, const T &obj) {
			s->emplace_back(obj, (s->empty() ? obj : min(obj, (s->back()).second)));
		}

	public:
		queue_min_ua(): 
			inp_obj_1(), inp_obj_2(), inp(&inp_obj_1), inp_tmp(&inp_obj_2), 
			out_obj_1(), out_obj_2(), out(&out_obj_1), out_tmp(&out_obj_2) 
		{}

		void turn() {
			switch (cur_mode) {
			case common:
				if (inp->size() < out->size() + 1)
					break;
				swap(inp, inp_tmp);
				cur_mode = itmp_otmp;
				// there is no break specially
			case itmp_otmp:
				push_out(out_tmp, (inp_tmp->back()).first);
				inp_tmp->pop_back();
				if (!inp_tmp->empty())
					break;
				cur_mode = out_otmp;
				// there is no break specially
			case out_otmp:
				if (out->empty()) {
					swap(out, out_tmp);
					cur_mode = common;
					break;
				}
				push_out(out_tmp, (out->front()).first);
				out->pop_front();
				if (out->empty()) {
					swap(out, out_tmp);
					cur_mode = common;
				}
			}
		}

		void push(const T &obj) {
			push_inp(inp, obj);
			turn(); // there it's last
		}

		void pop() {
			out->pop_back();
			turn(); // there it's first
		}

		const T &front() {
			return (out->back()).first;
		}

		const T &min() const {
			const T *result = &(out->back()).second; // out can't be empty if all container isn't

			if (!out_tmp->empty() && comp((out_tmp->back()).second, *result)) {
				result = &(out_tmp->back()).second;
			}
			if (!inp->empty() && comp((inp->back()).second, *result)) {
				result = &(inp->back()).second;
			}
			if (!inp_tmp->empty() && comp((inp_tmp->back()).second, *result)) {
				result = &(inp_tmp->back()).second;
			}
			return *result;
		}
	
		void clear() {
			cur_mode = common;
			inp->clear();
			inp_tmp->clear();
			out->clear();
			out_tmp->clear();
		}
	};
}

#endif // _QUEUE_MIN_UA_HPP_
