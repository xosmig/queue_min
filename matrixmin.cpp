/*
contest:
150921_au
task J
https://acm.math.spbu.ru/trains/150921_au.pdf
TL 36 (of 36 tests)
*/

#include <cstdio>
#include <vector>
#include <functional>
#include <deque>
#include <iostream>

using namespace std;

template<class T>
struct mstack {
	T *a = static_cast<T*>(operator new(1001 * sizeof(T)));
	size_t r = 0;

	template<class ... Args>
	void emplace_back(Args ... args) {
		new (a + r) T(args...);
		++r;
	}
	void pop_back() {
		--r;
	}
	void clear() {
		r = 0;
	}
	size_t size() {
		return r;
	}
	bool empty() {
		return r == 0;
	}
	T &back() {
		return a[r - 1];
	}
};

template<class T>
struct mdeque : public mstack<T> {
	size_t l = 0;
	void pop_front() {
		++l;
	}
	T &front() {
		return this->a[l];
	}
	void clear() {
		l = this->r = 0;
	}
	size_t size() {
		return this->r - l;
	}
	bool empty() {
		return l == this->r;
	}
};

template<
	class T,
	class Compare = std::less<T>,
	class Stack = std::deque<std::pair<const T, const T>>, // must have push_back, back, clear
	class Deque = std::deque<std::pair<const T, const T>> // must have push_back, back, push_front, front, clear
>
class queue_min_ua {
	Stack inp_obj_1, inp_obj_2, *inp, *inp_tmp;
	Deque out_obj_1, out_obj_2, *out, *out_tmp;

	Compare comp;

	enum mode {
		common,
		itmp_otmp,
		out_otmp
	} cur_mode;

	const T &min(const T &x, const T &y) const { // min of 2
		if (comp(x, y)) {
			return x;
		} else {
			return y;
		}
	}

	void push_out(Deque *d, const T &obj) { // inline by default
		d->emplace_back(obj, (d->empty() ? obj :min(obj, (d->back()).second)));
	}

	void push_inp(Stack *s, const T &obj) {
		s->emplace_back(obj, (s->empty() ? obj : min(obj, (s->back()).second)));
	}

public:
	queue_min_ua(): 
		inp_obj_1(), inp_obj_2(), inp(&inp_obj_1), inp_tmp(&inp_obj_2), 
		out_obj_1(), out_obj_2(), out(&out_obj_1), out_tmp(&out_obj_2)
	{}

	void turn() {
		switch (cur_mode) {
		case common:
			// assert(inp_tmp->empty());
			// assert(out_tmp->empty());
			if (inp->size() < out->size() + 1)
				break;
			swap(inp, inp_tmp);
			cur_mode = itmp_otmp;
			// there is no break specially
		case itmp_otmp:
			push_out(out_tmp, (inp_tmp->back()).first);
			inp_tmp->pop_back();
			if (!inp_tmp->empty())
				break;
			cur_mode = out_otmp;
			// there is no break specially
		case out_otmp:
			if (out->empty()) {
				swap(out, out_tmp);
				cur_mode = common;
				break;
			}
			push_out(out_tmp, (out->front()).first);
			out->pop_front();
			if (out->empty()) {
				swap(out, out_tmp);
				cur_mode = common;
			}
		}
	}

	void push(const T &obj) {
		push_inp(inp, obj);
		turn();
	}

	void pop() {
		out->pop_back();
		turn();
	}

	const T &front() {
		return (out->back()).first;
	}

	const T &min() const {
		const T *result = &(out->back()).second; // out can't be empty if all container isn't

		if (!out_tmp->empty() && comp((out_tmp->back()).second, *result)) {
			result = &(out_tmp->back()).second;
		}
		if (!inp->empty() && comp((inp->back()).second, *result)) {
			result = &(inp->back()).second;
		}
		if (!inp_tmp->empty() && comp((inp_tmp->back()).second, *result)) {
			result = &(inp_tmp->back()).second;
		}
		return *result;
	}

	void clear() {
		cur_mode = common;
		inp->clear();
		inp_tmp->clear();
		out->clear();
		out_tmp->clear();
	}
};

int main() {

	#define TASK "matrixmin"

	// freopen("test.in", "r", stdin);

	freopen(TASK".in", "r", stdin);
	freopen(TASK".out", "w", stdout);

	ios_base::sync_with_stdio(0);
	cin.tie(0);
 
	size_t n, m;
	cin >> n >> m;

	size_t k = n - m + 1;

	vector<vector<int> > a(n, vector<int>(n));

	for (auto &cur : a) {
		for (auto &x : cur) {
			cin >> x;
		}
	}

	queue_min_ua<int, less<int>, mstack<pair<const int, const int>>, mdeque<pair<const int, const int>>> q;

	for (size_t i = 0; i < n; ++i) { /// build line mins
		q.clear();
		for (size_t j = 0; j < m; ++j) {
			q.push(a[i][j]);
		}
		a[i][0] = q.min();
		for (size_t j = 1; j < k; ++j) {
			q.push(a[i][j + m - 1]);
			q.pop();
			a[i][j] = q.min();
		}
	}

	for (size_t j = 0; j < k; ++j) { /// build matrix mins by columns
		q.clear();
		for (size_t i = 0; i < m; ++i) {
			q.push(a[i][j]);
		}
		a[0][j] = q.min();
		for (size_t i = 1; i < k; ++i) {
			q.push(a[i + m - 1][j]);
			q.pop();
			a[i][j] = q.min();
		}
	}

	for (size_t i = 0; i < k; ++i) {
		for (size_t j = 0; j < k; ++j) {
			cout << a[i][j] << ' ';
		}
		cout << '\n';
	}
}
